/*
Asunder

Copyright(C) 2005 Eric Lathrop <eric@ericlathrop.com>
Copyright(C) 2007 Andrew Smith <http://littlesvr.ca/contact.php>

Any code in this file may be redistributed or modified under the terms of
the GNU General Public Licence as published by the Free Software
Foundation; version 2 of the licence.

*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include <gtk/gtk.h>

#include "support.h"
#include "main.h"

/* set in doRip() first */
bool overwriteAll;
bool overwriteNone;

bool confirmOverwrite(const char* pathAndName)
{
    GtkWidget* dialog;
    GtkWidget* label;
    GtkWidget* checkbox;
    char* lastSlash;
    int rc;
    char msgStr[1024];

    if(overwriteAll)
        return true;
    if(overwriteNone)
        return false;

    dialog = gtk_dialog_new_with_buttons(_("Overwrite?"),
                                         GTK_WINDOW(win_main),
                                         GTK_DIALOG_DESTROY_WITH_PARENT,
                                         "_Ok",
                                         GTK_RESPONSE_ACCEPT,
                                         "_No",
                                         GTK_RESPONSE_REJECT,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_REJECT);

    lastSlash = strrchr(pathAndName, '/');
    lastSlash++;

    snprintf(msgStr, 1024, _("The file '%s' already exists. Do you want to overwrite it?\n"), lastSlash);

    label = gtk_label_new(msgStr);
    gtk_widget_show(label);
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), label, TRUE, TRUE, 0);

    checkbox = gtk_check_button_new_with_mnemonic(_("Remember the answer for _all the files made from this CD"));
    gtk_widget_show(checkbox);
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), checkbox, TRUE, TRUE, 0);

    rc = gtk_dialog_run(GTK_DIALOG(dialog));

    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbox)))
    {
        if(rc == GTK_RESPONSE_ACCEPT)
            overwriteAll = true;
        else
            overwriteNone = true;
    }

    gtk_widget_destroy(dialog);

    if(rc == GTK_RESPONSE_ACCEPT)
        return true;
    else
        return false;
}

GtkWidget*
lookup_widget                          (GtkWidget       *widget,
                                        const gchar     *widget_name)
{
  GtkWidget *parent, *found_widget;

  for (;;)
    {
      if (GTK_IS_MENU (widget))
        parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
      else
        parent = gtk_widget_get_parent(widget);
      if (!parent)
        parent = (GtkWidget*) g_object_get_data (G_OBJECT (widget), "GladeParentKey");
      if (parent == NULL)
        break;
      widget = parent;
    }

  found_widget = (GtkWidget*) g_object_get_data (G_OBJECT (widget),
                                                 widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}

