Source: asunder
Section: sound
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Matthias Geiger <matthias.geiger1024@tutanota.de>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 meson (>= 0.49.0),
 libgtk-3-dev (>= 3.11.0),
 libcddb2-dev (>= 0.9.5),
 intltool
Standards-Version: 4.6.1
Homepage: https://gitlab.gnome.org/Salamandar/asunder
Vcs-Browser: https://salsa.debian.org/debian/asunder
Vcs-Git: https://salsa.debian.org/debian/asunder.git

Package: asunder
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, cdparanoia, vorbis-tools
Recommends: flac, wavpack
Suggests: lame
Description: graphical audio CD ripper and encoder
 Asunder is a graphical Audio CD ripper and encoder. It can be used to
 save tracks from Audio CDs. Main features:
 .
  * Supports WAV, MP3, Ogg Vorbis, FLAC, and Wavpack audio files
  * Uses CDDB to name and tag each track
  * Can encode to multiple formats in one session
  * Creates M3U playlists
  * Allows for each track to be by a different artist
  * Does not require a specific desktop environment (just GTK+)
